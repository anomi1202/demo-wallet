package main

import (
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/zerolog/log"
	"gitlab.ru/rkosykh/custom_gun/ammo"
	"google.golang.org/grpc/status"
)

func checkNoErr(err error, handler ammo.Handler) int {
	if err != nil {
		code := runtime.HTTPStatusFromCode(status.Code(err))

		log.Error().Err(err).Msgf("got error from handler '%s'", handler)
		return code
	}

	return http.StatusOK
}
