include makefile.dc.mk
include makefile.build.mk
include makefile.lint.mk
include makefile.migration.mk
include makefile.loadtest.mk

GO_COVER_EXCLUDE="config|service.go"
PG_DSN="host=localhost port=6432 dbname=demo_wallet user=postgres password=postgres sslmode=disable"

.PHONY: run
run:
	PG_DSN=$(PG_DSN) \
	go run ./cmd/demo-wallet/main.go -debug

.PHONY: test
test:
	PG_DSN=$(PG_DSN) \
	gotestsum --junitfile unit-tests.xml \
		--jsonfile json-report.txt \
		-- -count 3 -p 3 -race -cover -coverpkg=./internal/... -coverprofile=cover.tmp.out -covermode atomic ./internal/...

.PHONY: e2e
e2e:
	PG_DSN=$(PG_DSN) \
	$(LOCAL_BIN)/gotestsum -f testname -- -count 3 -p 3 ./test/...

.PHONY: test-cov
test-cov:
	make test
	grep -vE ${GO_COVER_EXCLUDE} cover.tmp.out > cover.out || cp cover.tmp.out cover.out
	go tool cover -func=cover.out
	go tool cover -html=cover.out


.PHONY: prom-refresh
prom-refresh:
	curl 'http://localhost:8428/-/reload'

.PHONY: prom-config
prom-config:
	curl 'http://localhost:8428/config'

.PHONY: prom-status
prom-status:
	 curl 'http://localhost:8428/api/v1/targets'|jq '.data.activeTargets| .[] | {pool:.scrapePool, status:.health}'
