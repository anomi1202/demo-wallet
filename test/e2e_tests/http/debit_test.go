package grpc

import (
	"context"
	"net/http"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.ru/rkosykh/demo-wallet/internal/pkg/domain"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	wallet "gitlab.ru/rkosykh/demo-wallet/test/utils/clients/http"
)

func TestDebit(t *testing.T) {
	t.Run("Positive cases", func(t *testing.T) {
		ctx := context.Background()

		account, err := mustCreateAccount(ctx, 100)
		require.NoError(t, err)

		debitRequest := &wallet.DebitRequest{
			AccountID:   account.AccountID,
			Amount:      100,
			OperationID: uuid.NewString(),
		}

		res, resp, err := client.Debit(ctx, debitRequest)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, resp.StatusCode)
		require.Equal(t, desc.OperationStatus_STATUS_OK.String(), res.Status)

		operations, err := store.GetAccountOperations(ctx, account.AccountID, 1000)
		require.NoError(t, err)
		require.Len(t, operations, 1)

		require.Equal(t, debitRequest.Amount, operations[0].Amount)
		require.Equal(t, debitRequest.OperationID, operations[0].OperationID)
		require.Equal(t, domain.OperationTypeDebit, operations[0].OperationType)
	})
}
