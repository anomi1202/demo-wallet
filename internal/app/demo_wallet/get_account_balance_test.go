package demowallet

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.ru/rkosykh/demo-wallet/internal/pkg/domain"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetBalance получить инфу по балансу
func TestGetAccountBalance(t *testing.T) {
	t.Run("Positive cases", func(t *testing.T) {
		for _, tt := range []struct {
			name    string
			balance int64
		}{
			{
				name:    "positive balance",
				balance: 100,
			}, {
				name:    "negative balance",
				balance: -100,
			}, {
				name:    "zero balance",
				balance: 0,
			},
		} {
			t.Run(tt.name, func(t *testing.T) {
				ctx := context.Background()

				account, err := mustCreateAccount(ctx, 100)
				require.NoError(t, err)

				impl := Implementation{store: store}

				balance, err := impl.GetAccountBalance(ctx, &desc.GetAccountBalanceRequest{
					AccountId: account.AccountID,
				})
				require.NoError(t, err)
				require.NotNil(t, balance)
				require.Equal(t, account.AccountID, balance.AccountId)
				require.Equal(t, account.Amount, balance.Amount)
			})
		}

	})

	t.Run("Negative cases", func(t *testing.T) {
		t.Run("Account doesn't exists", func(t *testing.T) {
			ctx := context.Background()

			impl := Implementation{store: store}

			balance, err := impl.GetAccountBalance(ctx, &desc.GetAccountBalanceRequest{
				AccountId: uuid.NewString(),
			})
			require.EqualError(t, err, status.Error(codes.Internal, "Get balance err: account not found").Error())
			require.Nil(t, balance)
		})
	})
}

func mustCreateAccount(ctx context.Context, amount int32) (*domain.Account, error) {
	account := &domain.Account{
		AccountID:   uuid.NewString(),
		Amount:      amount,
		Description: "test account",
	}
	err := store.AddAccount(ctx, account)
	if err != nil {
		return nil, err
	}

	return account, nil
}
