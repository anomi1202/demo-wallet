package grpc

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
	wallet "gitlab.ru/rkosykh/demo-wallet/test/utils/clients/http"
)

func TestCreateAccount(t *testing.T) {
	t.Run("Positive cases", func(t *testing.T) {
		ctx := context.Background()

		req := &wallet.CreateAccountRequest{
			Amount:      100,
			Description: "some desc",
		}

		res, resp, err := client.CreateAccount(ctx, req)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, resp.StatusCode)
		require.NotNil(t, resp)
		require.NotEmpty(t, res.AccountID)
		require.Equal(t, res.Amount, req.Amount)
		require.Equal(t, res.Description, req.Description)
	})
}
