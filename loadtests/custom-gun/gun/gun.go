package main

import (
	"context"

	"github.com/rs/zerolog/log"
	"github.com/spf13/afero"
	"github.com/yandex/pandora/cli"
	phttp "github.com/yandex/pandora/components/phttp/import"
	"github.com/yandex/pandora/core"
	"github.com/yandex/pandora/core/aggregator/netsample"
	coreimport "github.com/yandex/pandora/core/import"
	"github.com/yandex/pandora/core/register"
	a "gitlab.ru/rkosykh/custom_gun/ammo"
	"gitlab.ru/rkosykh/custom_gun/utils"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	"google.golang.org/grpc"
)

// GunConfig .
type GunConfig struct {
	Target string `validate:"required"` // Configuration will fail, without target defined
}

// Gun .
type Gun struct {
	// Configured on construction.
	client grpc.ClientConn
	conf   GunConfig
	ctx    context.Context
	// Configured on Bind, before shooting
	Aggr core.Aggregator // May be your custom Aggregator.
	core.GunDeps
}

// NewGun .
func NewGun(conf GunConfig) *Gun {
	return &Gun{
		conf: conf,
		ctx:  context.Background(),
	}
}

// Bind - подготовка к стрельбам
func (g *Gun) Bind(aggr core.Aggregator, deps core.GunDeps) error {
	conn, err := utils.InitDialContext(g.ctx, g.conf.Target)
	if err != nil {
		log.Error().Err(err).Msgf("Can't init connect to '%s'", g.conf.Target)
		return err
	}

	g.client = *conn // nolint
	g.Aggr = aggr
	g.GunDeps = deps

	return nil
}

// Shoot - один выстрел пушки
func (g *Gun) Shoot(ammo core.Ammo) {
	customAmmo := ammo.(*a.Ammo) // Shoot will panic on unexpected ammo type. Panic cancels shooting.

	g.shoot(customAmmo)
}

func (g *Gun) shoot(ammo *a.Ammo) {
	conn := g.client // nolint
	client := desc.NewDemoWalletClient(&conn)

	var code int
	sample := netsample.Acquire(ammo.Tag)

	switch ammo.Tag {
	case a.CreateAccount:
		code = g.CreateAccount(g.ctx, client, ammo)
	case a.GetAccountBalance:
		code = g.GetAccountBalance(g.ctx, client, ammo)
	case a.Debit:
		code = g.Debit(g.ctx, client, ammo)
	case a.Credit:
		code = g.Credit(g.ctx, client, ammo)
	default:
		code = 404
	}

	defer func() {
		sample.SetProtoCode(code)
		g.Aggr.Report(sample)
	}()
}

func main() {
	//debug.SetGCPercent(-1)

	// Standard imports.
	fs := afero.NewOsFs()
	coreimport.Import(fs)
	// May not be imported, if you don't need http guns and etc.
	phttp.Import(fs)

	// Custom imports. Integrate your custom types into configuration system.
	coreimport.RegisterCustomJSONProvider("custom_provider", func() core.Ammo { return &a.Ammo{} })

	register.Gun("custom_gun", NewGun, func() GunConfig {
		return GunConfig{}
	})

	cli.Run()
}
