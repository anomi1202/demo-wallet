package demowallet

import (
	context "context"
	"fmt"

	"github.com/google/uuid"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// Debit поступление денег
func (i *Implementation) Debit(ctx context.Context, req *desc.DebitRequest) (*desc.DebitResponse, error) {
	operationID := req.GetOperationId()
	if operationID == "" {
		operationID = uuid.NewString()
	}
	err := i.store.Debit(ctx, req.GetAccountId(), req.GetAmount(), operationID)
	if err != nil {
		return &desc.DebitResponse{
			Status: desc.OperationStatus_STATUS_FAIL,
		}, status.Error(codes.Internal, fmt.Sprintf("Debit process err: %v", err))
	}

	return &desc.DebitResponse{
		Status: desc.OperationStatus_STATUS_OK,
	}, nil
}
