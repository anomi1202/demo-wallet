package storage

import (
	"context"
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
	"gitlab.ru/rkosykh/demo-wallet/internal/pkg/domain"
)

// Storage - интерфейс для работы с бд
type Storage interface {
	AddAccount(ctx context.Context, account *domain.Account) error
	GetAccount(ctx context.Context, accountID string) (*domain.Account, error)
	Debit(ctx context.Context, accountID string, amount int32, operationID string) error
	Credit(ctx context.Context, accountID string, amount int32, operationID string) error
	GetAccountOperations(ctx context.Context, accountID string, limit uint64) ([]domain.AccountOperation, error)
}

type storage struct {
	db *sqlx.DB
}

// New .
func New(db *sqlx.DB) Storage {
	return &storage{
		db: db,
	}
}

// Builder вернет squirrel SQL Builder объект
func (s *storage) Builder() sq.StatementBuilderType {
	return sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
}

func (s *storage) DoInTransaction(ctx context.Context, db *sqlx.DB, fn func(ctx context.Context, tx *sqlx.Tx) error) (err error) {

	tx, err := db.BeginTxx(ctx, nil)
	if err != nil {
		return fmt.Errorf("can't begin transaction: %w", err)
	}

	defer func() {
		if r := recover(); r != nil {
			rollbackTx(ctx, tx)
		} else if err != nil {
			rollbackTx(ctx, tx)
		} else {
			if err = tx.Commit(); err != nil {
				err = fmt.Errorf("cannot commit transaction: %w", err)
			}
		}
	}()

	err = fn(ctx, tx)

	return err
}

func rollbackTx(_ context.Context, tx *sqlx.Tx) {
	if err := tx.Rollback(); err != nil {
		log.Error().Err(err).Msg("sqltx/rollbackTx: cannot rollback transaction")
	}
}
