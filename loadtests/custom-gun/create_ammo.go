package main

import (
	"flag"
	"fmt"
	"os"
	"path"

	"github.com/rs/zerolog/log"
	"gitlab.ru/rkosykh/custom_gun/ammo"
)

func main() {
	ammoPath := flag.String("ammo", "custom.ammo", "path to ammo file")
	ammoCount := flag.Int64("ammoCount", 1000, "ammo count")
	flag.Parse()

	file, err := createFile(*ammoPath)
	if err != nil {
		log.Fatal().Err(err).Msg("can't create ammo file")
	}

	if err := ammo.Generate(*ammoCount, file); err != nil {
		log.Fatal().Err(err).Msg("Failed create account")
	}
}

func createFile(fileName string) (*os.File, error) {
	localPath, err := os.Getwd()
	if err != nil {
		return nil, fmt.Errorf("can't get current directory: %w", err)
	}

	filePath := path.Join(localPath, fileName)
	if _, err = os.Stat(filePath); err == nil {
		//file exists. remove it
		if err = os.Remove(filePath); err != nil {
			return nil, fmt.Errorf("failed removing old ammo file: %w", err)
		}
	}

	file, err := os.Create(filePath) // nolint:gosec
	if err != nil {
		return nil, fmt.Errorf("can't create ammo file: %w", err)
	}

	return file, nil
}
