db-create-migration: NAME=$NAME
db-create-migration:
	goose -dir ./migrations postgres $(PG_DSN) create "${NAME}" sql

db-status: $(eval $(call db_env))
	goose -dir ./migrations postgres $(PG_DSN) status

db-up: $(eval $(call db_env))
	goose -dir ./migrations postgres $(PG_DSN) up

db-down: $(eval $(call db_env))
	goose -dir ./migrations postgres $(PG_DSN) down
