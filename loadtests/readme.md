# Часть 1: Мониторинг

```shell
make dc-app-observability-up
```

## Grafana

1. Настраиваем источник данных: **Configuration→ Data Sources->Add DataSource**
2. Выбираем тип **DataSource -> Prometheus** - Так как VictoriaMetrics - полностью имплементит Prometheus API 
3. Заполняем источник данных:
   1. Имя оставляем как есть - **Prometheus**
   2. Указываем адрес: http://victoria-metrics:8428
      - данные тянутся через back-end - так что необходимо указать доменное имя внутри docker-network
   3. SaveAndTest - Должен появится зеленый квадратик.
4. Настраиваем Dashboard

### [Grafana-Dashboard for VictoriaMetrics](https://grafana.com/grafana/dashboards/10229-victoriametrics/)

1. `+` -> Import dashboard 
2. Вводим в поле `Import via grafana.com`: 10229 
3. Load -> Import


### [Node Exporter](https://grafana.com/grafana/dashboards/1860-node-exporter-full/)
    
1. `+` -> Import dashboard 
2. Вводим в поле `Import via grafana.com`: 1860 
3. Load -> Import 


### [Postgres exporter](https://grafana.com/grafana/dashboards/9628-postgresql-database/)
    
1. `+` -> Import dashboard 
2. Вводим в поле `Import via grafana.com`: 9628 
3. Load -> Import


### Pgbouncer

1. `+` -> Import Dashboard
2. Импортируем конфиг борды `grafana/pg-bouncer-dashboard.json`
3. Сохраняем борду


### Yandex tank

1. `+` -> Import Dashboard
2. Импортируем конфиг борды `grafana/yandex-tank-dashboard.json`
3. Сохраняем борду


### Добавляем мониторинг demo-wallet

1. `+` -> Import Dashboard
2. Импортируем конфиг борды `grafana/demo-wallet-dashboard.json`
3. Сохраняем борду

---

# Часть 2: Yandex-Tank + Pandora

Документация по секциям Yandex-tank: https://yandextank.readthedocs.io/en/latest/config_reference.html

## Предварительные условия

Образы для предварительного скачивания:

````shell
make loadtest-images
````

## HTTP pandora gun

```shell
make loadtest-http
```

## gRPC pandora gun

```shell
make loadtest-grpc
```
