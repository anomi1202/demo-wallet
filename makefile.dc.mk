DC_DB:=docker-compose -f docker-compose.db.yaml -p demo_wallet
DC_APP:=docker-compose -f docker-compose.app.yaml -f docker-compose.db.yaml -p demo_wallet
DC_OBSERVABILITY:=docker-compose -f docker-compose.app.yaml -f docker-compose.db.yaml -f docker-compose.observability.yaml -p demo_wallet

dc-db-up:
	$(DC_DB) up -d

dc-db-down:
	$($(DC_DB)) down --remove-orphans -v -t0

dc-db-reup:
	make dc-db-down
	make dc-db-up

###########
dc-app-build: dc-app-down
	$(DC_APP) up -d --no-deps --build --force-recreate demo-wallet

dc-app-up:
	$(DC_APP) up -d
	sleep 1
	make dc-app-status

dc-app-down:
	$(DC_APP) down --remove-orphans -v -t0
	make dc-app-status

dc-app-reup: dc-app-down dc-app-up

dc-app-rebuild-reup: dc-app-build dc-app-up

###########

dc-app-observability-up:
	$(DC_OBSERVABILITY) up -d
	make dc-app-status

dc-app-observability-down:
	$(DC_OBSERVABILITY) down --remove-orphans -v -t0
	make dc-app-status

dc-app-observability-reup: dc-app-observability-down dc-app-observability-up

###########

dc-app-restart:
	$(DC_OBSERVABILITY) up -d pgboucer demo-wallet
	make dc-app-status

dc-app-status:
	sleep 1
	docker-compose -p demo_wallet ps -a

dc-logs:
	docker-compose -p demo_wallet logs demo-wallet

###########

colima:
	colima stop
	colima start --cpu 2 --memory 6 --disk 50
	colima list
	docker ps -a
